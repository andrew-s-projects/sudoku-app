package com.example.sudoku2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.BadParcelableException;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.GridView;

public class MainActivity extends AppCompatActivity {
    MyCanvas myCanvas;
    BoardView boardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App app = new App();
        setContentView(R.layout.activity_main);

        boardView = new BoardView(this);

        myCanvas = new MyCanvas(this);

        App.setBoard(boardView);

        // findViewId elements have to be initialized first
        BoardView view1 = (BoardView) findViewById(R.id.boardView);

        App.setID(view1);
    }

    // process buttons 1 - 9 for user input on screen
    public void processButton(View view){

        int id = view.getId();

        switch (id){
            case R.id.number1:
                boardView.newInput(1);
                break;
            case R.id.number2:
                boardView.newInput(2);
                break;
            case R.id.number3:
                boardView.newInput(3);
                break;
            case R.id.number4:
                boardView.newInput(4);
               // App.setPressed(num);
                break;
            case R.id.number5:
                boardView.newInput(5);
                break;
            case R.id.number6:
                boardView.newInput(6);
                break;
            case R.id.number7:
                boardView.newInput(7);
                break;
            case R.id.number8:
                boardView.newInput(8);
                break;
            case R.id.number9:
                boardView.newInput(9);
                break;
            default:
                throw new RuntimeException("Button not found");
        }

    }
    public void solvePuzzle(View view){

        while(true){
            if(App.getBackend().forwardChecking()){
                System.out.println("Solved");
                break;
            }
        }

    }
    public void undoAction(View view){
        App.getBackend().undoAction();

    }
    public void clearAction(View view){
        App.getBackend().clearBoard();

    }

}

