package com.example.sudoku2;

import java.util.ArrayList;

public class Cell {


    private ArrayList<Integer> domain;
    int col;
    int row;
    int assignment;

    public Cell(int col, int row, int assignment){

        this.col = col;
        this.row = row;
        this.assignment = assignment;
        domain = new ArrayList<>();

    }

    public int getAssignment() {
        return assignment;
    }
    public void setAssignment(int x){assignment = x;}

    public int getCol(){
        return col;
    }
    public int getRow(){
        return row;
    }

    public ArrayList getDomain(){
        return domain;
    }
}
