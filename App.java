package com.example.sudoku2;

import android.app.Application;
import android.content.Context;
import android.graphics.Point;

// deals with communication between backend and main activity
public class App extends Application {

    private static  Context context;
    static int pressed;
    static BoardBack backend;
    static BoardView board;
    private static boolean set;
    static BoardView id;

    public void onCreate(){
        super.onCreate();
        App.context = getApplicationContext();
        backend = new BoardBack();
        board = new BoardView(App.getAppContext());
    }
    public static Context getAppContext(){
        return App.context;
    }
    public static void setPressed(int num){
        pressed = num;

    }
    public static int getPressed(){
        return pressed;
    }
    public static BoardBack getBackend(){
        return backend;
    }
    public static void setBackend(BoardBack back){
        backend = back;
        set = true;
    }
    public static void setBoard(BoardView b){
        board = b;
    }
    public static BoardView getBoard(){
        return board;
    }
    public static void  setID(BoardView a){
        id = a;
    }
    public static BoardView getID(){
        return id;
    }
    public static boolean getSet(){
        return set;
    }



}
