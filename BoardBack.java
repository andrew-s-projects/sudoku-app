package com.example.sudoku2;

import android.content.Context;
import android.graphics.Point;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

// class that takes care of solving algorithm and board logic

public class BoardBack {

    ArrayList<Point> drawn_numbers;
    Stack<Integer> assigned_variables; // list containing inputted variables
    ArrayList<Cell> cells; // a list of cells on board
    ArrayList<Integer> col_visited; // list of placed numbers in column
    ArrayList<Integer> row_visited; // list of placed numbers in row
    ArrayList<Integer> block_visited; // list of placed numbers in block
    ArrayList<Integer> default_domain; // list containing possible choices for each cell (1-9)
    ArrayList<Point> last_cell; // the last cell to be placed on board
    ArrayList<Cell> current_state; // the current state of board (input numbers)
    ArrayList<ArrayList<Cell>> the_states; // list of repeating states
    ArrayList<Point> last_assigned; // list of assigned user values


    Point active_cell;
    int input;
    public int chosen_cell;
    boolean back = false;


    public BoardBack() {
        drawn_numbers = new ArrayList<>();
        cells = new ArrayList<>();
        default_domain = new ArrayList<>();
        current_state = new ArrayList<>();
        the_states = new ArrayList<ArrayList<Cell>>();
        last_cell = new ArrayList<>();
        col_visited = new ArrayList<>();
        row_visited = new ArrayList<>();
        block_visited = new ArrayList<>();
        assigned_variables = new Stack<>();
        last_assigned = new ArrayList<Point>();
        chosen_cell = 0;
    }

    public void setDrawn_numbers(Point p) {
        drawn_numbers.add(p);
    }

    public ArrayList get_DrawnNumbers() {
        return drawn_numbers;
    }

    public void setInput(int num) {
        input = num;
    }

    public int getInput() {
        return input;
    }

    public ArrayList getCells() {
        return cells;
    }

    // sets active cell = whatever cell the user pressed on board
    public void setActive_cell(int col, int row) {
        // active_cell = null means no cell is pressed on board

        if (col == -1 && row == -1) {
            active_cell = null;
        } else {
            active_cell = new Point(col, row);

        }
    }

    public Point getActive_cell() {
        return active_cell;
    }

    // initialize all the cells on the board
    public void initializeCells() {

        // 9 x 9 board - total of 81 cells

        // first create the cells
        assigned_variables.push(0);
        for (int i = 0; i < 81; i++) {

            // calculate row, col of cell
            int row = i / 9;
            int col = i % 9;

            cells.add(new Cell(col, row, 0));
            current_state.add(new Cell(col, row, 0));

        }

        // then create the domain for each cell ( 1 - 9)

        for (int i = 0; i < cells.size(); i++) {


            for (int j = 1; j <= 9; j++) {
                cells.get(i).getDomain().add(j);
            }
        }

        // initialize the default domain (used in updateDomain method)

        for (int i = 1; i <= 9; i++) {
            default_domain.add(i);
        }
    }

    // updates the assignment of cell

    public void assignCell(int col, int row, int num) {

        // if input number is not available to assign (not in cells domain), don't assign it
        // this avoids possibly of duplicate value in row, col, block

        if(cells.get(findCellInList(col, row)).getDomain().contains(num)){

            // first check to see if we've selected a cell

            // find cell in question and change its assignment to input number
            cells.get(findCellInList(col, row)).setAssignment(num);

            int assigned = findCellInList(col, row);

            assigned_variables.add(assigned);
            current_state.get(findCellInList(col,row)).setAssignment(num);
            last_assigned.add(new Point(col, row));
            // check to see if board satisfies constraint

            if (goalCheck()) {
            } else {
                updateDomain();
            }
        }

    }

    // implementation of undoing an action
    public void undoAction(){

        // only do this if a number has actually been assigned
        if(assigned_variables.size() > 0){

            int x = last_assigned.get(last_assigned.size() - 1).x;
            int y = last_assigned.get(last_assigned.size() - 1).y;

            //last_cell.remove(last_cell.size() - 1);
            cells.get(findCellInList(x, y)).setAssignment(0);
            current_state.get(findCellInList(x, y)).setAssignment(0);
            last_assigned.remove(last_assigned.size() - 1);
            setActive_cell(0 , 0 );
            updateDomain();
        }
    }

    // find the given cell in the list of cells
    private int findCellInList(int col, int row) {

        return (row * 9) + col % 81;

    }
    public int getChosenCell(){
        return chosen_cell;
    }
    public void printAssigned(){
        for(int i = 0; i < assigned_variables.size(); i++){
            System.out.print(assigned_variables.get(i));
        }
        System.out.println();
    }


    // clears board - when the user wants to restart
    public void clearBoard() {
        drawn_numbers = new ArrayList<>();
        cells = new ArrayList<>();
        default_domain = new ArrayList<>();
        current_state = new ArrayList<>();
        the_states = new ArrayList<ArrayList<Cell>>();
        last_cell = new ArrayList<>();
        col_visited = new ArrayList<>();
        row_visited = new ArrayList<>();
        block_visited = new ArrayList<>();
        assigned_variables = new Stack<>();
        chosen_cell = 0;
        last_assigned = new ArrayList<>();
        initializeCells();
        App.getID().postInvalidate();
    }

    // shuffles domains of cells
    private void shuffleDomain() {

        for (int i = 0; i < cells.size(); i++) {
            Collections.shuffle(cells.get(i).getDomain());
        }
    }

    public boolean goalCheck() {
        int passed_number = 0; //the number must pass 3 cases - no duplicates in target row, column, block
        int total_passed = 0; // if total passed is 81, then we have reached the goal state
        int goal_state = 0;
        int check_row = 0;
        int check_col = 0;
        int block_start = 0; // variable to keep track of starting column of block
        int block_pass = 0;
        int size = 0;

        for (int i = 0; i < 81; i++) {

            passed_number = 0;
            int col = i % 9;
            int row = i / 9;
            check_row = 0;
            check_col = 0;
            block_pass = 0;

            Cell target_Cell = cells.get(findCellInList(col, row));

            if (findCellInList(col, row) != 0) {
                size++;
            }
            // check to see if rows satisfy constraint
            int target = target_Cell.getAssignment();

            for (int j = 0; j < 9; j++) {
                if (cells.get(findCellInList(j, row)).getAssignment() != target) {
                    check_row++;
                }
            }
            // if 9 numbers pass in row, entire row satisfies constraint
            if (check_row == 8) {
                passed_number++;
            }
            // check to see if columns satisfy constraint
            for (int z = 0; z < 9; z++) {
                if (cells.get(findCellInList(col, z)).getAssignment() != target) {
                    check_col++;
                }
            }

            // if 9 numbers pass in column, entire column satisfies constraint
            if (check_col == 8) {
                passed_number++;
            }

            // check to se if block satisfies constraint

            int startRow_no = (row / 3) * 3;
            int startCol_no = (col / 3) * 3;
            int col_range = startCol_no + 3;
            int row_range = startRow_no + 3;

            for (int x = startRow_no; x < row_range; x++) {
                for (int q = startCol_no; q < col_range; q++) {
                    if (cells.get(findCellInList(q, x)).getAssignment() != target) {
                        block_pass++;
                    }
                }
            }

            // if 9 numbers pass in block, the block satisfies the constraint
            if (block_pass == 8) {

                passed_number++;
            }

            if (passed_number == 3) {
                // number passes all three cases - correct position
                total_passed++;
            }

        }
        if (total_passed == 81) {
            // each number meets constraint - goal state met
            return true;
        }
        return false;

    }

    public void updateDomain() {

        for (int i = 0; i < 81; i++) {
            int col = i % 9;
            int row = i / 9;

            int the_row = 0;
            int the_col = 0;

            int assigned_value = cells.get(findCellInList(col, row)).getAssignment();

            if (i == 0 && last_cell.size() > 0) {

                the_row = last_cell.get(last_cell.size() - 1).y;
                the_col = last_cell.get(last_cell.size() - 1).x - 1;

                int startRow_no = (the_row / 3) * 3;
                int startCol_no = (the_col / 3) * 3;
                int col_range = startCol_no + 3;
                int row_range = startRow_no + 3;

                // get a list of visited columns thus far

                for (int x = the_col; x >= 0; x--) {
                    col_visited.add(cells.get(findCellInList(the_row, x)).getAssignment());
                }

                // get a list of visited rows (in same column)
                for (int x = the_row; x >= 0; x--) {
                    if (cells.get(findCellInList(x, the_col + 1)).getAssignment() != 0) {
                        row_visited.add(cells.get(findCellInList(x, the_col + 1)).getAssignment());
                    }
                }

                for (int x = startRow_no; x < row_range; x++) {
                    for (int q = startCol_no; q < col_range; q++) {
                        if (cells.get(findCellInList(x, q)).getAssignment() != 0 && cells.get(findCellInList(x, q)).getAssignment() !=
                                cells.get(findCellInList(the_row, the_col + 1)).getAssignment()) {
                            block_visited.add(cells.get(findCellInList(x, q)).getAssignment());
                        }
                    }
                }
            }

            // if we are back tracking, try to update domains accordingly
            if (back) {

                int the_column = the_col + 1;

                if (col == 8) {

                    update_visited(row + 1, the_col, true);

                }

                // if we back tracked to first column of cell (i.e column 3 or 6), need to change current block
                //
                if (last_cell.get(last_cell.size() - 1).x == 3 || last_cell.get(last_cell.size() - 1).x == 6) {

                    update_visited(the_row, the_col + 1, false);
                    the_col = the_col + 1;

                }

                // see if we can add any numbers back to domains in same row
                for (int j = 0; j < 9; j++) {
                    for (int element : default_domain) {

                        // test
                        int temp_row = j;

                        // if element is not in row, column or block or in domain, re-add to domains
                        //if (test_vec.size() == 0) {
                        if (!cells.get(findCellInList(j, the_column)).getDomain().contains(element)) {
                            if (!block_visited.contains(element) && !col_visited.contains(element) && !row_visited.contains(element)) {
                                cells.get(findCellInList(j, the_column)).getDomain().add(element);

                            }
                        }
                    }
                }

                // see if we can add any numbers back to domains in same column
                for (i = 0; i < 9; i++) {

                    for (int element : default_domain) {

                        //if (test_row.size() == 0) {

                        if (!cells.get(findCellInList(the_row, i)).getDomain().contains(element)) {
                            if (!block_visited.contains(element) && !col_visited.contains(element) && !row_visited.contains(element)) {
                                cells.get(findCellInList(the_row, i)).getDomain().add(element);

                            }
                        }
                    }
                }

                // see if we can add any numbers back to domains in same block
                int ro = (the_row / 3) * 3;
                int co = (the_col / 3) * 3;
                int co_range = co + 3;
                int ro_range = ro + 3;

                for (int x = ro; x < ro_range; x++) {

                    for (int q = co; q < co_range; q++) {

                        for (int element : default_domain) {

                            if (!cells.get(findCellInList(x, q)).getDomain().contains(element)) {

                                if (!block_visited.contains(element) && !col_visited.contains(element) && !row_visited.contains(element)) {
                                    cells.get(findCellInList(x, q)).getDomain().add(element);

                                }
                            }
                        }
                    }
                }
                // remove this cell from list of visited cells
                if (last_cell.size() > 0) {
                    last_cell.remove(last_cell.size() - 1);
                }
                // not backtracking anymore
                back = false;


            }

            // in case of no backtrack, remove just inputted number from appropriate domains (from remaining cellls)

            if (assigned_value != 0 && back != true) {


                // remove input number from all domains in column
                for (int j = 0; j < 9; j++) {

                    for (int u = 0; u < cells.get(findCellInList(j, row)).getDomain().size(); u++) {

                        int test = Integer.parseInt(cells.get(findCellInList(j, row)).getDomain().get(u).toString());

                        // if we encounter non-assigned cell and input number is in domain, remove it
                        // repeat this procedure for rows, blocks

                        if (test == assigned_value && cells.get(findCellInList(j, row)).getAssignment() == 0) {
                            cells.get(findCellInList(j, row)).getDomain().remove(u);
                        }
                    }
                }

                // remove input number from all domains in row
                for (int z = 0; z < 9; z++) {

                    for (int u = 0; u < cells.get(findCellInList(col, z)).getDomain().size(); u++) {

                        int test = Integer.parseInt(cells.get(findCellInList(col, z)).getDomain().get(u).toString());

                        if (test == assigned_value) {
                            cells.get(findCellInList(col, z)).getDomain().remove(u);
                        }
                    }
                }

                // remove input number from all domains in block
                int startRow_no = (row / 3) * 3;
                int startCol_no = (col / 3) * 3;
                int col_range = startCol_no + 3;
                int row_range = startRow_no + 3;

                for (int x = startRow_no; x < row_range; x++) {
                    for (int q = startCol_no; q < col_range; q++) {

                        for (int u = 0; u < cells.get(findCellInList(q, x)).getDomain().size(); u++) {

                            int test = Integer.parseInt(cells.get(findCellInList(q, x)).getDomain().get(u).toString());
                            if (test == assigned_value) {
                                cells.get(findCellInList(q, x)).getDomain().remove(u);
                            }
                        }
                    }
                }
            }
        }

        col_visited.clear();
        row_visited.clear();
        block_visited.clear();
        App.getID().postInvalidate();
    }

    private void update_visited(int row, int column, boolean flag) {

        row_visited.clear();
        block_visited.clear();


        for (int i = row; i >= 0; i--) {
            row_visited.add(cells.get(findCellInList(i, column)).getAssignment());
            // if row is not 0, work up and add columns from earlier rows
        }

        int startRow_no = (row / 3) * 3;
        int startCol_no = (column / 3) * 3;
        int col_range = startCol_no + 3;
        int row_range = startRow_no + 3;

        for (int x = startRow_no; x < row_range; x++) {

            for (int q = startCol_no; q < col_range; q++) {

                if (cells.get(findCellInList(x, q)).getAssignment() != 0) {
                    block_visited.add(cells.get(findCellInList(x, q)).getAssignment());
                }
            }
        }
        if (block_visited.size() == 0) {
            block_visited.add(0);
        }
        if (flag) {
            col_visited.clear();
            for (int i = 0; i < 9; i++) {
                col_visited.add(cells.get(findCellInList(row, i)).getAssignment());
            }
        }
    }

    private int goBack() {

        return 0;
    }

    // debugging
    private void printNormal(ArrayList<Cell> x){

        for(int i = 0; i < x.size(); i++){
            System.out.print(x.get(i).getAssignment());
        }
        System.out.println();
    }

    // attempts to find a given state in the list of states
    private int findTempState(ArrayList<Cell> temp) {

        ArrayList<Integer> temp_list = new ArrayList();

        // for every state in the_states, compare it to temp
        // if equal to temp, increase count - no reason to use list - don't want to change it now
        for (int i = 0; i < the_states.size(); i++) {
            if (compareState(the_states.get(i), temp)) {
                temp_list.add(1);
            }
        }
        if(temp_list.isEmpty()){
            return 0;
        }
        else{
            return temp_list.size();
        }
    }

    // helper method to determine if two states (list of cells) are equal - compares each cell's assignment
    private boolean compareState(ArrayList<Cell> state1, ArrayList<Cell> state2) {

        for (int i = 0; i < 81; i++) {

            int row = i / 9;
            int col = i % 9;

            int cell_no = (row * 9) + col % 81;


            // if the states are not equal, return false
            if (state1.get(i).getAssignment() != state2.get(i).getAssignment()) {
                return false;
            }
        }
        // if the states are equal return true
        return true;
    }

    // MANUALLY make a deep copy of lists - cuz java sucks
    private ArrayList<Cell> cloneLists(ArrayList<Cell> list1){

        ArrayList<Cell> temp = new ArrayList<>();

        for(int i = 0; i < list1.size(); i++){
            temp.add(new Cell(list1.get(i).getCol(), list1.get(i).getRow(), list1.get(i).getAssignment()));
        }

        return temp;
    }

    boolean forwardChecking() {

        updateDomain();

        // check to see if there are empty domains first, if so, we need to back track

        for (int i = 0; i < 81; i++) {

            int y = i / 9;
            int x = i % 9;

            //if cell hasn't been assigned and its domain size is 0, begin backtrack

            if (current_state.get(findCellInList(y, x)).getAssignment() == 0 && cells.get(findCellInList(y, x)).getDomain().size() == 0) {

                int available_assignment = 0;

                while (available_assignment == 0) {
                    int current_id = assigned_variables.peek();

                    int row = current_id / 9;
                    int col = current_id % 9;

                    cells.get(findCellInList(row, col)).setAssignment(0);
                    current_state.get(findCellInList(row, col)).setAssignment(0);
                    back = true;

                    updateDomain();
                    for (int z = 0; z < cells.get(findCellInList(row, col)).getDomain().size(); z++) {

                        ArrayList<Cell> temp_state = cloneLists(current_state);

                        temp_state.get(findCellInList(row, col)).setAssignment(
                                Integer.parseInt(cells.get(findCellInList(row, col)).getDomain().get(z).toString()));

                        // if not in the_states

                        if (findTempState(temp_state) == 0) {

                            current_state = cloneLists(temp_state);
                            cells.get(findCellInList(row, col)).setAssignment(
                                    Integer.parseInt(cells.get(findCellInList(row, col)).getDomain().get(z).toString()));

                            the_states.add(temp_state);
                            available_assignment = 1;
                            chosen_cell = current_id;
                            last_cell.add(new Point(col, row));
                            updateDomain();
                            return false;
                        }
                    }
                    if (available_assignment == 0) {

                        cells.get(findCellInList(y, x)).setAssignment(0);
                        current_state.get(findCellInList(y, x)).setAssignment(0);
                        assigned_variables.pop();
                    }

                }
            }
        }

        int count = 0;
        while (count < 81) {

            int min_index = 0;
            int min_number = 10;

            for (int i = 0; i < 81; i++) {

                int y = i / 9;
                int x = i % 9;

                if (current_state.get(findCellInList(y, x)).getAssignment() == 0 && cells.get(findCellInList(y, x)).getDomain().size() > 0) {
                    if (cells.get(findCellInList(y, x)).getDomain().size() < min_number) {
                        min_index = i;
                        min_number = cells.get(findCellInList(y, x)).getDomain().size();
                    }
                }
            }
            int y = min_index / 9;
            int x = min_index % 9;

            if (current_state.get(findCellInList(y, x)).getAssignment() == 0 && cells.get(findCellInList(y, x)).getDomain().size() > 0) {
                // System.out.println("Im here");
                int min_id = 0;
                current_state.get(findCellInList(y, x)).setAssignment(
                        Integer.parseInt(cells.get(findCellInList(y, x)).getDomain().get(min_id).toString()));
                cells.get(findCellInList(y, x)).setAssignment(
                        Integer.parseInt(cells.get(findCellInList(y, x)).getDomain().get(min_id).toString()));

                assigned_variables.push(min_index);

                ArrayList<Cell> the_temp = cloneLists(current_state);

                the_states.add(the_temp);

                chosen_cell = 9 * y + x;

                last_cell.add(new Point(x, y));

                updateDomain();
                return false;
            }
            count++;

        }
        if (goalCheck()) {
            return true;
        } else {
            int available_assignment = 0;
            while (available_assignment == 0) {

                int current_id = assigned_variables.peek();
                int y = current_id / 9;
                int x = current_id % 9;
                cells.get(findCellInList(y, x)).setAssignment(0);
                current_state.get(findCellInList(y, x)).setAssignment(0);
                updateDomain();

                for (int i = 0; i < cells.get(findCellInList(y, x)).getDomain().size(); i++) {

                    ArrayList<Cell> temp_state = cloneLists(current_state);

                    temp_state.get(findCellInList(y, x)).setAssignment(
                            Integer.parseInt(cells.get(findCellInList(y, x)).getDomain().get(i).toString()));

                    if (findTempState(temp_state) == 0) {
                        current_state = temp_state;
                        cells.get(findCellInList(y, x)).setAssignment(
                                Integer.parseInt(cells.get(findCellInList(y, x)).getDomain().get(i).toString()));
                        the_states.add(current_state);
                        available_assignment = 1;

                        // chosen_cell = cur_id;

                        chosen_cell = current_id;

                        last_cell.add(new Point(x, y));
                        break;
                    }
                }
                if (available_assignment == 0) {
                    assigned_variables.pop();
                }
            }
            return false;
        }
    }
}