package com.example.sudoku2;

import android.app.Activity;
import android.os.Build;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.*;
import android.graphics.Point;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import java.util.ArrayList;


public class BoardView extends View{


    private Paint paint; // painting lines
    private Paint paint_text; // painting text
    private Rect rect;
    private Canvas the_canvas;
    private BoardBack backend;
    private float cell_size = 0; // the size of each cell on the board
    private float cell_length = 0; // the length of each cell on the board
    private  int size = 9;


    public BoardView(Context context) {
        super(context);
        init(null);
    }

    public BoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public BoardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public BoardView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init(attrs);
    }

    private void init(@Nullable AttributeSet set){

        // initialize objects
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint_text = new Paint(Paint.ANTI_ALIAS_FLAG);
        rect = new Rect();
        backend = new BoardBack();
        if(App.getSet() != true){
            App.setBackend(backend);
        }

        // paint configurations for line elements
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(10); //15
        paint.setStyle(Paint.Style.STROKE);

        // paint configuration for Text elements
        paint_text.setColor(Color.BLACK);
        paint_text.setStyle(Paint.Style.FILL);
        paint_text.setTextSize(getResources().getDimension(R.dimen.text_size));

        App.getBackend().initializeCells();

    }
    @Override
    protected void onDraw(Canvas canvas){

        the_canvas = canvas;
        // draw initial rectangle (board border) and finish drawing lines
        canvas.drawRect(0F, 0F, canvas.getWidth(), canvas.getHeight(), paint);
        drawLines(canvas);

        // check if we need to draw user input (assignment at cell will not be 0)

        ArrayList<Cell> cells = App.getBackend().getCells();


        for (int i = 0; i < 81; i++){

            Cell the_cell = cells.get(i);
            //System.out.println(the_cell.getAssignment());
            if(the_cell.getAssignment() > 0){
                updateCell(the_cell.col, the_cell.row, the_cell.getAssignment());
            }
        }
    }

    // used to draw cells deliminated by thinner lines between outer box
    private void drawLines(Canvas canvas){

        paint.setStrokeWidth(3); // update line width

        //draw vertical lines
        cell_size = canvas.getWidth() / size; // start line draw at this x coordinate

        cell_length = canvas.getHeight() / size;

        float start_y = 0;
        float end_y = canvas.getHeight(); // the ending y coordinate of drawn line
        float start_x = 0;

        // draw vertical lines
        for (int i = 0; i < size; i++){


            if (i % 3 == 0 && i != 0){
                paint.setStrokeWidth(10); //10

            }
            else{
                paint.setStrokeWidth(3);
            }
            canvas.drawLine( cell_size * i , start_y, cell_size * i , end_y, paint);
            canvas.drawLine(0, cell_length * (i) , canvas.getWidth(), cell_length * (i)  , paint);

        }
        if(App.getBackend().getInput() > 0  && App.getBackend().getActive_cell() != null){
            highlightBox();
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event){



        if(event.getAction() == MotionEvent.ACTION_DOWN){

            // delay until keyboard user input

            Point temp = new Point(findCell(event.getX(), event.getY()));
            App.getBackend().setActive_cell(temp.x, temp.y);
            App.getBackend().setInput(1);
            // wait for user input from keyboard, if input, update board
            //highlightBox(); // highlight selected cell
            App.getID().postInvalidate();

        }
        return true;
    }

    // highlight the selected cell on board

    private void highlightBox(){

        int col = App.getBackend().getActive_cell().x;
        int row = App.getBackend().getActive_cell().y;


        // get location y coordinate of view
        BoardView view1 = (BoardView) findViewById(R.id.boardView);
        int [] location = new int[2];
        view1.getLocationOnScreen(location);
        int top = location[1];

        // set paint attributes
        Paint highlight = new Paint();
        highlight.setStyle(Paint.Style.STROKE);
        highlight.setStrokeWidth(5);
        highlight.setColor(Color.parseColor("#00BCD4"));

        // draw rect around selected cell
        the_canvas.drawRect(col * cell_size, (row * cell_length) , (col + 1) * cell_size, ((row + 1) * cell_length), highlight);

    }

    // find pressed cell - used for backend purposes
    private Point findCell(float touch_x, float touch_y){

        // these are the row, columns of touched cell
        int cell_col = 0;
        int cell_row = 0;


        float start_x = 0; // we will start our search at first cell
        float start_y = 0; // we will start out search at first row

        // find the touched column on board
        for (int i = 0; i < 9; i++){

            start_x = cell_size * i; // traverse each cell

            // first time our touched x cord is les than cell width, touched cell is equal to i - 1
            if(touch_x < start_x){
                cell_col = i - 1;
                break;
            }
            // reached end of board and cell not found
            else if(touch_x > start_x && i == 8){
                cell_col = 8;
            }
            else{continue;}
        }

        // find the touched row on board

        for (int j = 0; j < 9; j++){

            start_y = cell_length * j;

            // first time out touched y cord is less than cell length, touched cell is equal to i - 1
           if(touch_y < start_y){
               cell_row = j - 1;
               break;
           }
           // reached end of board and cell not found
            else if (touch_y > start_y && j == 8){
               cell_row = 8;
           }
            else{
                continue;
           }
        }

        return new Point(cell_col, cell_row);
    }

    // used to draw points based on user input on postInvalidate()
    private void updateCell(int col, int row, int num){


        // use col, row (cell) to compute position of number (center of cell)
        float x_pos = col * cell_size + (cell_size / 4);
        float y_pos = row * cell_length + (cell_size);

        // draw on canvas and re-draw view
        the_canvas.drawText(Integer.toString(num), x_pos, y_pos, paint_text);


    }

    //process new user input from keyboard
    public void newInput(int num){

        // if we've actually selected a cell
        if(App.getBackend().getActive_cell() != null){
            App.getBackend().assignCell(App.getBackend().getActive_cell().x, App.getBackend().getActive_cell().y, num);
            // set selected cell to inactive until user clicks a cell on board
            App.getBackend().setActive_cell(-1, -1);
        }
       App.getID().postInvalidate();

    }

}
